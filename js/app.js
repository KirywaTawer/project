window.addEventListener("DOMContentLoaded", () => {

    // Tabs
    const tabs = document.querySelectorAll(".tabheader__item");

    const tabsContent = document.querySelectorAll(".tabcontent");

    const tabsParent = document.querySelector(".tabheader__items");

    function hideTabContent() {
        tabsContent.forEach(item => {
            item.style.display = "none";
        });
        tabs.forEach(item => {
            item.classList.remove("tabheader__item_active");
        });
    };

    function showTabContent(i = 0) {
        tabsContent[i].style.display = "block";
        tabs[i].classList.add("tabheader__item_active")
    }

    hideTabContent();
    showTabContent();

    tabsParent.addEventListener("click", (event) => {
        const target = event.target
        if (target && target.classList.contains('tabheader__item')) {
            tabs.forEach((item, i) => {
                if (target == item) {
                    hideTabContent();
                    showTabContent(i);
                }
            })
        }
    })

    //

    // Timer

    const deadline = new Date(2023, 06, 01);

    let timerId = null;

    function declensionNum(num, words) {
        return words[(num % 100 > 4 && num % 100 < 20) ? 2 : [2, 0, 1, 1, 1, 2][(num % 10 < 5) ? num % 10 : 5]];
    }

    function countdownTimer() {
        const diff = deadline - new Date();
        if (diff <= 0) {
            clearInterval(timerId);
        }
        const days = diff > 0 ? Math.floor(diff / 1000 / 60 / 60 / 24) : 0;
        const hours = diff > 0 ? Math.floor(diff / 1000 / 60 / 60) % 24 : 0;
        const minutes = diff > 0 ? Math.floor(diff / 1000 / 60) % 60 : 0;
        const seconds = diff > 0 ? Math.floor(diff / 1000) % 60 : 0;
        $days.textContent = days < 10 ? '0' + days : days;
        $hours.textContent = hours < 10 ? '0' + hours : hours;
        $minutes.textContent = minutes < 10 ? '0' + minutes : minutes;
        $seconds.textContent = seconds < 10 ? '0' + seconds : seconds;
        $days.dataset.title = declensionNum(days, ['день', 'дня', 'дней']);
        $hours.dataset.title = declensionNum(hours, ['час', 'часа', 'часов']);
        $minutes.dataset.title = declensionNum(minutes, ['минута', 'минуты', 'минут']);
        $seconds.dataset.title = declensionNum(seconds, ['секунда', 'секунды', 'секунд']);
    }

    const $days = document.querySelector('#days');
    const $hours = document.querySelector('#hours');
    const $minutes = document.querySelector('#minutes');
    const $seconds = document.querySelector('#seconds');
    countdownTimer();
    timerId = setInterval(countdownTimer, 1000);

    //

    // ModalWindow

    const btn = document.querySelectorAll(".btn")

    let modalWindow = document.querySelector(".modal")

    const modalClose = document.querySelector(".modal__close")

    function closeModal() {
        modalClose.addEventListener("click", () => {
            modalWindow.style.display = "none"
            document.body.style.overflow = "visible"
        });
    }

    closeModal()

    function modalWindowFunc() {
        modalWindow.style.display = "block"
        document.body.style.overflow = "hidden"
        clearInterval(modalTimerId)
    };

    btn.forEach((item, i) => {
        btn[i].addEventListener("click", () => {
            modalWindowFunc()
        });
    });

    // const modalTimerId = setTimeout(modalWindowFunc, 5000)

    function showModalByScroll() {
        if (window.pageYOffset + document.documentElement.clientHeight >= document.documentElement.scrollHeight) {
            modalWindowFunc();
            window.removeEventListener("scroll", showModalByScroll)
        }
    };

    window.addEventListener("scroll", showModalByScroll);

    // classes for cards

    class MenuCara {
        constructor(src, alt, title, descr, price, parentSelector, ...classes) {
            this.src = src;
            this.alt = alt;
            this.title = title;
            this.descr = descr;
            this.price = price;
            this.classes = classes;
            this.parent = document.querySelector(parentSelector)
            this.transfer = 27;
            this.changetoUAN();
        }
        changetoUAN() {
            this.price = this.price * this.transfer;
        }

        render() {
            const element = document.createElement('div');
            if (this.classes.length === 0) {
                this.element = "menu__item"
                element.classList.add(this.element);
            } else {
                this.classes.forEach(className => element.classList.add(className));
            }
            element.innerHTML = `
                    <img src=${this.src} alt=${this.alt}>
                    <h3 class="menu__item-subtitle">${this.title}</h3>
                    <div class="menu__item-descr">${this.descr}</div>
                    <div class="menu__item-divider"></div>
                    <div class="menu__item-price">
                    <div class="menu__item-cost">Цена:</div>
                    <div class="menu__item-total"><span>${this.price}</span> грн/день</div>
                </div>
            `;
            this.parent.append(element);
        }
    }
    new MenuCara(
        "img/tabs/vegy.jpg",
        "xuy",
        "sdfdsfsd",
        "fvfdvfxvsdvxcvfdvxcvvdv",
        3333,
        ".menu .container",
    ).render();

    new MenuCara(
        "img/tabs/vegy.jpg",
        "xuy",
        "sdfdsfsd",
        "fvfdvfxvsdvxcvfdvxcvvdv",
        3333,
        ".menu .container",
        "menu__item"
    ).render();

    new MenuCara(
        "img/tabs/vegy.jpg",
        "xuy",
        "sdfdsfsd",
        "fvfdvfxvsdvxcvfdvxcvvdv",
        3333,
        ".menu .container",
        'menu__item'
    ).render();

    //

    //changeColorBtn
    const changebtn = document.querySelector(".calculating__choose-item")

    const changebtnMan = document.querySelector(".man__man")

    changebtn.addEventListener("click", () => {
        if (changebtn.classList.contains("woman")) {
            changebtn.classList.toggle("calculating__choose-item__active")
            changebtnMan.classList.remove("calculating__choose-item__active")
        }
    })
    changebtnMan.addEventListener("click", () => {
        if (changebtnMan.classList.contains("man")) {
            changebtnMan.classList.toggle("calculating__choose-item__active")
            changebtn.classList.remove("calculating__choose-item__active")
        }
    })

    const height = document.querySelector("#height"),
        weight = document.querySelector("#weight"),
        age = document.querySelector("#age");

    height.addEventListener("click", () => {
        removeHeight()
    });
    weight.addEventListener("click", () => {
        removeWeight()
    });
    age.addEventListener("click", () => {
        removeAge()
    });

    function removeAge() {
        age.classList.toggle("calculating__choose-item__active");
        weight.classList.remove("calculating__choose-item__active");
        height.classList.remove("calculating__choose-item__active");
    }
    function removeWeight() {
        weight.classList.toggle("calculating__choose-item__active");
        height.classList.remove("calculating__choose-item__active");
        age.classList.remove("calculating__choose-item__active");
    }
    function removeHeight() {
        height.classList.toggle("calculating__choose-item__active");
        weight.classList.remove("calculating__choose-item__active");
        age.classList.remove("calculating__choose-item__active");
    }

    const low = document.querySelector("#low"),
          small = document.querySelector("#small"),
          medium = document.querySelector("#medium"),
          high = document.querySelector("#high");

          
    low.addEventListener("click", () => {
        removeLow()
    });

    small.addEventListener("click", () => {
        removeSmall()
    });

    medium.addEventListener("click", () => {
        removeMedium()
    });

    high.addEventListener("click", () => {
        removeHigh()
    });

    function removeLow(){
        low.classList.toggle("calculating__choose-item__active");
        small.classList.remove("calculating__choose-item__active");
        medium.classList.remove("calculating__choose-item__active");
        high.classList.remove("calculating__choose-item__active");
    }
    function removeSmall(){
        low.classList.remove("calculating__choose-item__active");
        small.classList.toggle("calculating__choose-item__active");
        medium.classList.remove("calculating__choose-item__active");
        high.classList.remove("calculating__choose-item__active");
    }
    function removeMedium(){
        low.classList.remove("calculating__choose-item__active");
        small.classList.remove("calculating__choose-item__active");
        medium.classList.toggle("calculating__choose-item__active");
        high.classList.remove("calculating__choose-item__active");
    }
    function removeHigh(){
        low.classList.remove("calculating__choose-item__active");
        small.classList.remove("calculating__choose-item__active");
        medium.classList.remove("calculating__choose-item__active");
        high.classList.toggle("calculating__choose-item__active");
    }
    //

    //slider

    
    //
});